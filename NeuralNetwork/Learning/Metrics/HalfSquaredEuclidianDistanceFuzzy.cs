﻿using System;
using NeuralNetwork.Learning.Interfaces;
using NeuralNetwork.Structure.AdditionalItems;


namespace NeuralNetwork.Learning.Metrics
{
    internal class HalfSquaredEuclidianDistanceFuzzy:IMetrics<ValuePair>
    {
        public ValuePair Calculate(ValuePair[] v1, ValuePair[] v2)
        {
            ValuePair V = new ValuePair(0, 0);
            double d = 0;
            for (int i = 0; i < v1.Length; i++)
            {
                V.Moda += (v1[i].Moda - v2[i].Moda) * (v1[i].Moda - v2[i].Moda);
                V.Func += (v1[i].Func - v2[i].Func) * (v1[i].Func - v2[i].Func);
            }
            V.Moda *= 0.5;
            V.Func *= 0.5;
            return V;
        }

        public ValuePair CalculatePartialDerivaitveByV2Index(ValuePair[] v1, ValuePair[] v2, int v2Index)
        {
            ValuePair V = new ValuePair(v2[v2Index].Moda - v1[v2Index].Moda,Math.Abs(v2[v2Index].Func - v1[v2Index].Func));
            return V;
        }
    }
}
