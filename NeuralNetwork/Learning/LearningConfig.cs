﻿using NeuralNetwork.Learning.Interfaces;

namespace NeuralNetwork.Learning
{
    public class LearningConfig
    {
        double _learningRate=1;
        int _batchSize=-1;
        double _regularizationFactor=1;
        int _maxEpoches=10000;
        double _maxError=0.1;
        double _maxErrorChange=0.01;
        IMetrics<double> _errorFunction;

        public LearningConfig() { }
        public LearningConfig(double learningRate, int maxEpoches)
        {
            _learningRate = learningRate;
            _maxEpoches = maxEpoches;
        }
        public LearningConfig(double learningRate, int maxEpoches,double maxError,double maxErrorChange)
        {
            _learningRate = learningRate;
            _maxEpoches = maxEpoches;
            _maxError = maxError;
            _maxErrorChange = maxErrorChange;
        }
        public LearningConfig(double learningRate, int maxEpoches, double maxError, double maxErrorChange,IMetrics<double> errorFunction)
        {
            _learningRate = learningRate;
            _maxEpoches = maxEpoches;
            _maxError = maxError;
            _maxErrorChange = maxErrorChange;
            _errorFunction = errorFunction;
        }
        public double LearningRate { get { return _learningRate; } set { _learningRate = value; } }
        /// <summary>
        /// Size of the batch. -1 means fullbatch size. 
        /// </summary>
        public int BatchSize { get { return _batchSize; } set { _batchSize = value; } }

        public double RegularizationFactor { get { return _regularizationFactor; } set { _regularizationFactor = value; } }

        public int MaxEpoches { get { return _maxEpoches; } set { _maxEpoches = value; } }

        /// <summary>
        /// If cumulative error for all training examples is less then MaxError, then algorithm stops 
        /// </summary>
        public double MaxError { get { return _maxError; } set { _maxError = value; } }

        /// <summary>
        /// If cumulative error change for all training examples is less then MaxErrorChange, then algorithm stops 
        /// </summary>
        public double MaxErrorChange { get { return _maxErrorChange; } set { _maxErrorChange = value; } }

        /// <summary>
        /// Function to minimize
        /// </summary>
        public IMetrics<double> ErrorFunction { get { return _errorFunction; } set { _errorFunction = value; } }

    }
}
