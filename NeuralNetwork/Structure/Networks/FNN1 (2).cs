﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using NeuralNetwork.Structure.Interfaces.Components;
using NeuralNetwork.Structure.Interfaces.Networks;
using NeuralNetwork.Structure.AdditionalItems;

namespace NeuralNetwork.Structure.Networks
{
    class FNN1:INeuralNetwork
    {
        ILayer[] _layers;
        public double[] ComputeOutput(double[] inputVector)
        {
            double[] M= inputVector;
            for(int i=0;i<_layers.Length;i++)
            {
                M = _layers[i].Compute(M);
            }
            return M;
        }
        /*public Stream Save()
        {
            Stream S = new StreamWriter();
            S.
        }*/
        public void Load(Stream S)
        {

        }
        public void Train(IList<DataItem<double>> data)
        {

        }
    }
}
