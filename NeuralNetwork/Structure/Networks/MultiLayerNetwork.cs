﻿using System;
using System.Collections.Generic;
using NeuralNetwork.Structure.Interfaces.Components;
using NeuralNetwork.Structure.AdditionalItems;
using System.IO;

namespace NeuralNetwork.Structure.Networks
{
    class MultiLayerNetwork
    {
        ILayer[] _layers;
        public double[] ComputeOutput(double[] inputVector)
        {
            double[] outputVector=_layers[0].Compute(inputVector);
            for(int i=1;i<_layers.Length;i++)
            {
                outputVector= _layers[0].Compute(outputVector);
            }
            return outputVector;
        }
        /*public Stream Save()
        {
            Stream S = new StreamWriter();
            S.
        }*/
        public void Load(Stream S)
        {

        }

        public void Train(IList<DataItem<ValuePair>> data)
        {

        }
        public ILayer[] Layers
        {
            get { return _layers; }
        }
    }
}
