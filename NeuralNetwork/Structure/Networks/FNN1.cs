﻿using System.Collections.Generic;
using System.IO;
using NeuralNetwork.Structure.Interfaces.Components;
using NeuralNetwork.Structure.Interfaces.Networks;
using NeuralNetwork.Structure.AdditionalItems;

namespace NeuralNetwork.Structure.Networks
{
    public class FNN1:IFMNN
    {
        ILayer[] _layers;
        public ValuePair[] ComputeOutput(ValuePair[] inputVector)
        {
            double[] m = new double[inputVector.Length];
            double[] f= new double[inputVector.Length];
            for(int i=0;i< inputVector.Length;i++)
            {
                m[i] = inputVector[i].Moda;
                f[i] = inputVector[i].Func;
            }
            for (int i = 0; i < _layers.Length; i++)
            {
                if(i%2==0)
                    m = _layers[i].Compute(m);
                else
                {
                    double[] fInput1 = new double[f.Length + m.Length];
                    for (int j = 0; j < f.Length; j++)
                        fInput1[j] = f[j];
                    for (int j = 0; j < m.Length; j++)
                        fInput1[f.Length+j] = m[j];
                    f = _layers[i].Compute(fInput1);
                }
            }
            ValuePair[] outputVector = new ValuePair[m.Length];
            for (int i = 0; i < inputVector.Length; i++)
            {
                outputVector[i] = new ValuePair(m[i], f[i]);
            }
            return outputVector;
        }
        /*public Stream Save()
        {
            Stream S = new StreamWriter();
            S.
        }*/
        public void Load(Stream S)
        {

        }

        public void Train(IList<DataItem<ValuePair>> data)
        {

        }
        public ILayer[] Layers
        {
            get { return _layers; }
        }
    }
}
