﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeuralNetwork.Structure.AdditionalItems
{
    public class ValuePair
    {
        double _moda;
        double _func;

        public ValuePair()
        {
        }

        public ValuePair(double moda, double func)
        {
            _moda = moda;
            if (Math.Abs(_func) > 1)
                throw (new Exception("Wrong func"));
            else _func = func;
        }

        public double Moda
        {
            get { return _moda; }
            set { _moda = value; }
        }

        public double Func
        {
            get { return _func; }
            set
            {
                if (Math.Abs(_func) > 1)
                    throw (new Exception("Wrong func"));
                else _func = value;
            }
        }
    }
}
