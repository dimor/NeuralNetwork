﻿using System.Collections.Generic;
using NeuralNetwork.Structure.Interfaces.Components;

namespace NeuralNetwork.Structure.Components
{
    public class Neuron : INeuron
    {
        double[] _weights;
        double _bias = 0;
        double lastNet = 0;
        double lastState = 0;
        IFunction _function;
        List<INeuron> _childs;
        List<INeuron> _parents;
        double dedz;
        public Neuron()
        {
            _childs = new List<INeuron>();
            _parents = new List<INeuron>();
            _function = new Components.Functions.SigmoidFunction();
        }
        public Neuron(List<INeuron> childs, List<INeuron> parents)
        {
            _childs = childs;
            _parents = parents;
            _function = new Components.Functions.SigmoidFunction();
        }
        public Neuron(List<INeuron> childs, List<INeuron> parents, double[] weights)
        {
            _childs = childs;
            _parents = parents;
            _weights = weights;
            _function = new Components.Functions.SigmoidFunction();
        }
        public Neuron(List<INeuron> childs, List<INeuron> parents, double[] weights,IFunction function)
        {
            _childs = childs;
            _parents = parents;
            _weights = weights;
            _function = function;
        }
        public void AddChild(INeuron neuron)
        {
            _childs.Add(neuron);
        }
        public void AddParent(INeuron neuron)
        {
            _parents.Add(neuron);
            double[] weights = new double[_parents.Count];
            for (int i = 0; i < _weights.Length; i++)
                weights[i] = _weights[i];
            _weights = weights;
        }
        public double[] Weights { get { return _weights; } }
        public double Bias
        {
            get { return _bias; }
            set { _bias = value; }
        }
        public double NET(double[] inputVector)
        {
            lastNet = 0;
            for (int i = 0; i < inputVector.Length; i++)
                lastNet += _weights[i] * inputVector[i];
            return lastNet;
        }
        public double Activate(double[] inputVector)
        {
            return lastState = _function.Compute(NET(inputVector));
        }
        public double LastState
        {
            get { return lastState; }
            set { lastState = value; }
        }
        public double LastNET
        {
            get { return lastNet; }
            set { lastNet = value; }
        }
        public IList<INeuron> Childs { get { return _childs; } }
        public IList<INeuron> Parents { get { return _parents; } }
        public IFunction ActivationFunction { get { return _function; } set { _function = value; } }
        public double dEdz { get { return dedz; } set { dedz = value; } }
    }
}
