﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NeuralNetwork.Structure.Interfaces.Components;

namespace NeuralNetwork.Structure.Components
{
    class Layer:ILayer
    {
        INeuron[] _neurons;
        double[] _lastOutput;
        public double[] Compute(double[] inputVector)
        {
            for (int i = 0; i < _neurons.Length; i++)
                _lastOutput[i] = _neurons[i].Activate(inputVector);
            return _lastOutput;
        }
        public double[] LastOutput { get { return LastOutput; } }
        public INeuron[] Neurons { get { return _neurons; } }
        public int InputDimension { get { return _neurons.Length; } }
    }
}
