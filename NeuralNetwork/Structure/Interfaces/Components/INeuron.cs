﻿using System.Collections.Generic;
using NeuralNetwork.Structure.Interfaces.Components;

namespace NeuralNetwork.Structure.Interfaces.Components
{
    public interface INeuron
    {
        /// <summary>
        /// Weights of the neuron
        /// </summary>
        double[] Weights { get; }

        /// <summary>
        /// Offset/bias of neuron (default is 0)
        /// </summary>
        double Bias { get; set; }

        /// <summary>
        /// Compute NET of the neuron by input vector
        /// </summary>
        /// <param name="inputVector">Input vector (must be the same dimension as was set in SetDimension)</param>
        /// <returns>NET of neuron</returns>
        double NET(double[] inputVector);

        /// <summary>
        /// Compute state of neuron
        /// </summary>
        /// <param name="inputVector">Input vector (must be the same dimension as was set in SetDimension)</param>
        /// <returns>State of neuron</returns>
        double Activate(double[] inputVector);

        /// <summary>
        /// Last calculated state in Activate
        /// </summary>
        double LastState { get; set; }

        /// <summary>
        /// Last calculated NET in NET
        /// </summary>
        double LastNET { get; set; }

        IList<INeuron> Childs { get; }

        IList<INeuron> Parents { get; }

        IFunction ActivationFunction { get; set; }

        double dEdz { get; set; }
    }
}
