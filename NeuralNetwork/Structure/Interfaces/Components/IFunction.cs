﻿namespace NeuralNetwork.Structure.Interfaces.Components
{
    public interface IFunction
    {
        double Compute(double x);
        double ComputeFirstDerivative(double x);
    }
}
