﻿using NeuralNetwork.Structure.Interfaces.Components;

namespace NeuralNetwork.Structure.Interfaces.Networks
{
    interface IFMNN:IFuzzyNeuralNetwork
    {
        /// <summary>
        /// Get array of layers of network
        /// </summary>
        ILayer[] Layers { get; }
    }
}
