﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeuralNetwork.Structure.Interfaces.Networks
{
    interface IMultiNNForFNN:IMultilayerNeuralNetwork
    {
        double[][] ComputeOutput(double[] inputVector);
    }
}
