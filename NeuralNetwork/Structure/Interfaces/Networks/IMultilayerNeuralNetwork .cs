﻿using NeuralNetwork.Structure.Interfaces.Components;

namespace NeuralNetwork.Structure.Interfaces.Networks
{
    public interface IMultilayerNeuralNetwork : INeuralNetwork
    {
        /// <summary>
        /// Get array of layers of network
        /// </summary>
        ILayer[] Layers { get; }
    }
}
