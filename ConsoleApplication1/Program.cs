﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NeuralNetwork.Structure.Networks;
using NeuralNetwork.Structure.AdditionalItems;
using NeuralNetwork.Structure.Components;
using NeuralNetwork.Learning;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            Neuron n = new Neuron();
            Layer l = new Layer(1);
            MultiLayerNetwork MLN = new MultiLayerNetwork(5, new int[] { 1, 5, 5, 5, 10 });
            List<DataItem<double>> test = new List<DataItem<double>>();
            List<DataItem<double>> check = new List<DataItem<double>>();
            check.Add(new DataItem<double>(new double[] { 0 }, new double[] { 0 }));
            for (int i=1;i<10;i++)
            {
                check.Add(new DataItem<double>(new double[] { Math.PI / (10 - i) }, new double[] { Math.Sin(Math.PI / (10 - i)) }));
            }
            for (int i = 1; i < 100; i++)
            {
                test.Add(new DataItem<double>(new double[] { Math.PI / (10 - i) }, new double[] { Math.Sin(Math.PI / (10 - i)) }));
            }
            MLN.Train(test);
        }
    }
}
