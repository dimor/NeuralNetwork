﻿using System.Collections.Generic;
using NeuralNetwork.Structure.AdditionalItems;
using System.IO;

namespace NeuralNetwork.Structure.Interfaces.Networks
{
    public interface INeuralNetwork
    {
        /// <summary>
        /// Compute output vector by input vector
        /// </summary>
        /// <param name="inputVector">Input vector (double[])</param>
        /// <returns>Output vector (double[])</returns>
        double[] ComputeOutput(double[] inputVector);

        //Stream Save();
        void Load(Stream S);
        /// <summary>
        /// Train network with given inputs and outputs
        /// </summary>
        /// <param name="inputs">Set of input vectors</param>
        /// <param name="outputs">Set if output vectors</param>
        void Train(IList<DataItem<double>> data);
    }
}
