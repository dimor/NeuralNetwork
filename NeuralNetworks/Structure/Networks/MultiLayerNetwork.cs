﻿using System;
using System.Collections.Generic;
using NeuralNetwork.Structure.Interfaces.Components;
using NeuralNetwork.Structure.Interfaces.Networks;
using NeuralNetwork.Structure.Components;
using NeuralNetwork.Structure.AdditionalItems;
using NeuralNetwork.Learning.Interfaces;
using NeuralNetwork.Learning;
using System.IO;

namespace NeuralNetwork.Structure.Networks
{
    public class MultiLayerNetwork:IMultilayerNeuralNetwork
    {
        Layer[] _layers;
        BackpropagationFCNLearningAlgorithm learnStrategy;
        public MultiLayerNetwork(int Layers,int[] NeuronsInLayers)
        {
            Random rnd = new Random();
            _layers = new Layer[Layers];
            _layers[0] = new Layer(NeuronsInLayers[0]);
            for (int j = 0; j < NeuronsInLayers[0]; j++)
            {
                _layers[0].AddNeuron(new Neuron(new double[] { 1 }));
            }
            for (int i = 1; i < Layers; i++)
            {
                _layers[i] = new Layer(NeuronsInLayers[i]);
                List<INeuron> parents = new List<INeuron>();
                for (int j = 0; j < _layers[i - 1].InputDimension; j++)
                {
                    parents.Add(_layers[i - 1].Neurons[j]);
                }
                for (int j = 0; j < NeuronsInLayers[i]; j++)
                    _layers[i].AddNeuron(new Neuron(parents,rnd));
            }
            LearningConfig config = new LearningConfig();
            learnStrategy = new BackpropagationFCNLearningAlgorithm(config);
        }
        public double[] ComputeOutput(double[] inputVector)
        {
            double[] outputVector = _layers[0].Compute(inputVector);
            for (int i = 1; i < _layers.Length; i++)
            {
                outputVector = _layers[0].Compute(outputVector);
            }
            return outputVector;
        }
        /*public Stream Save()
        {
            Stream S = new StreamWriter();
            S.
        }*/
        public void Load(Stream S)
        {

        }

        public void Train(IList<DataItem<double>> data)
        {
            learnStrategy.Train(this, data);
        }
        public ILayer[] Layers
        {
            get { return _layers; }
        }
    }
}
