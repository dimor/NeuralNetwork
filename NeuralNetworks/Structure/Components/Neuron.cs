﻿using System;
using System.Collections.Generic;
using NeuralNetwork.Structure.Interfaces.Components;
using NeuralNetwork.Structure.Components;

namespace NeuralNetwork.Structure.Components
{
    public class Neuron : INeuron
    {
        double[] _weights;
        double _bias = 0;
        double lastNet = 0;
        double lastState = 0;
        IFunction _function=new Functions.SigmoidFunction();
        //List<INeuron> _childs;
        List<INeuron> _parents;
        double dedz;
        public Neuron()
        {
            //_childs = new List<INeuron>();
            _parents = new List<INeuron>();
        }
        public Neuron(double[] weights):base()
        {
            _weights = weights;
        }
        public Neuron(List<INeuron> parents, Random rnd)
        {
            //_childs = childs;
            _parents = parents;
            _weights = new double[_parents.Count];
            for (int k = 0; k < _weights.Length; k++)
                _weights[k] = rnd.Next(100) / 1000;
        }
        public Neuron(List<INeuron> parents, double[] weights)
        {
            _parents = parents;
            _weights = weights;
        }
        public Neuron(List<INeuron> parents, double[] weights, IFunction function)
        {
            _parents = parents;
            _weights = weights;
            _function = function;
        }
        public void AddParent(INeuron neuron)
        {
            _parents.Add(neuron);
            double[] weights = new double[_parents.Count];
            for (int i = 0; i < _weights.Length; i++)
                weights[i] = _weights[i];
            _weights = weights;
        }
        public double[] Weights { get { return _weights; } }
        public double Bias
        {
            get { return _bias; }
            set { _bias = value; }
        }
        public double NET(double[] inputVector)
        {
            lastNet = 0;
            for (int i = 0; i < inputVector.Length; i++)
                lastNet += _weights[i] * inputVector[i];
            return lastNet;
        }
        public double Activate(double[] inputVector)
        {
            lastState = _function.Compute(NET(inputVector));
            return lastState;
        }
        public double LastState
        {
            get { return lastState; }
            set { lastState = value; }
        }
        public double LastNET
        {
            get { return lastNet; }
            set { lastNet = value; }
        }
        //public IList<INeuron> Childs { get { return _childs; } }
        public IList<INeuron> Parents { get { return _parents; } }
        public IFunction ActivationFunction { get { return _function; } set { _function = value; } }
        public double dEdz { get { return dedz; } set { dedz = value; } }
    }
}
