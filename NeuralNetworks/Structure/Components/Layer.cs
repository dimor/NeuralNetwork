﻿using NeuralNetwork.Structure.Interfaces.Components;

namespace NeuralNetwork.Structure.Components
{
    public class Layer : ILayer
    {
        INeuron[] _neurons;
        double[] _lastOutput;
        int Count = 0;
        public Layer(int Neurons)
        {
            _neurons = new INeuron[Neurons];
            _lastOutput = new double[Neurons];
        }
        public void AddNeuron(INeuron neuron)
        {
            _neurons[Count] = neuron;
            Count++;
        }
        public double[] Compute(double[] inputVector)
        {
            for (int i = 0; i < _neurons.Length; i++)
                _lastOutput[i] = _neurons[i].Activate(inputVector);
            return _lastOutput;
        }
        public double[] LastOutput { get { return LastOutput; } }
        public INeuron[] Neurons { get { return _neurons; } }
        public int InputDimension { get { return _neurons[0].Weights.Length; } }
    }
}
