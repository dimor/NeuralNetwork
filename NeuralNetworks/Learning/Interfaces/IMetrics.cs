﻿namespace NeuralNetwork.Learning.Interfaces
{
    public interface IMetrics<T>
    {
        T Calculate(T[] v1, T[] v2);

        /// <summary>
        /// Calculate value of partial derivative by v2[v2Index]
        /// </summary>
        T CalculatePartialDerivaitveByV2Index(T[] v1, T[] v2, int v2Index);
    }
}
