﻿using System.Collections.Generic;
using NeuralNetwork.Structure.AdditionalItems;

namespace NeuralNetwork.Learning.Interfaces
{
    public interface ILearningStrategy<T1, T2>
    {
        /// <summary>
        /// Train neural network
        /// </summary>
        /// <param name="network">Neural network for training</param>
        /// <param name="inputs">Set of input vectors</param>
        /// <param name="outputs">Set of output vectors</param>
        void Train(T1 network, IList<DataItem<T2>> data);
    }
}
